<?php

namespace HandyMama\Controllers;

use HandyMama\Models\Lead;
use HandyMama\Models\Client;
use HandyMama\Models\Customer;
use HandyMama\Models\Job;
use HandyMama\Models\Rating;
use HandyMama\Models\Tablerule;
use HandyMama\Models\Subscribe;

class Lead_controller{

  //SMS api auth
  private $sms_api_username = "handymama";
  private $sms_api_pw = "handy2016mama";

//create_new_lead==============================Start|
  public function create_new_lead($request, $response, $args){


    $data = $request->getParsedBody();


    $service = $data['service'];
    $sub_service = $data['sub_service'];//Pay attention
    $order_data = $data['order_data'];//Pay attention
    $user_name = $data['user_name'];
    $user_phone = $data['user_phone'];
    // $user_another_phone = filter_var($data['user_another_phone'], FILTER_SANITIZE_STRING);
    $user_email = $data['user_email'];
    $street_address = $data['street_address'];
    $house_number = $data['house_number'];
    $area = $data['area'];


    if($request->getAttribute('has_errors')){
      //Validation failed==========================================
      $errors = $request->getAttribute('errors');
        return $response->withStatus(400)->withJson($errors);
    } else {
      //Validation Passed===========================================

      //$create_new_lead_result will contain order post success or failure response
      $create_new_lead_result = array();

      //Only save in client table if the user is not registered yet================Start
      $client = new Client();
      //I need this data latter, during sms sending that is why i have stored the value in $is_client_exists
      $is_client_exists = $client->is_client_exists($user_phone);
      if($is_client_exists === false){

        //Generate a random password for this client
        $client_pw = $this->random_password();

        //Generate a verification code for this client
        $verification_code = $this->client_verification_code_generator();

        $client->firstname = $user_name;
        $client->password = md5($client_pw);
        $client->email = $user_email;
        $client->phone = $user_phone;
        $client->street = $house_number . ", " . $street_address;
        $client->area = $area;
        $client->sms_code = $verification_code;

        //$client_saved will hold boolean data if the model saved or not
        $client_saved = $client->save();

        if(!$client_saved){
          //For some reason data is not inserted in clients table
          $create_new_lead_result['fail'] = "fail to post the order- please try again";
          return $response->withStatus(400)->withJson($create_new_lead_result);
        }else{
          // $last_inserted_client_id = $client->id;
        }

      }
      //Only save in client table if the user is not registered yet================End

      //Only save in customer table if the data is not present there yet================Start
      $customer = new Customer();
      if($customer->is_customer_exists($user_phone) === false){

        $customer->name = $user_name;
        $customer->phone1 = $user_phone;
        $customer->email = $user_email;
        $customer->street1 = $house_number . ", " . $street_address;
        $customer->area1 = $area;

        $customer_saved = $customer->save();

        //get customer id
        if($customer_saved){

          $lead_data_cid = $customer->cid;//Not validated
        }else{
          //For some reason data is not inserted in customers table
          $create_new_lead_result['fail'] = "fail to post the order- please try again";
          return $response->withStatus(400)->withJson($create_new_lead_result);
        }


      }else{
        //already customer exists so take his id to insert into leads table
        $single_customer = $customer->get_single_customer_by_column_name('phone1', $user_phone);
        $lead_data_cid = $single_customer->cid;
      }
      //Only save in customer table if the data is not present there yet================End

      //Leads table==================================_start|
      $lead = new Lead();
      $lead->cid = $lead_data_cid;
      $lead->services = $service;
      $lead->subservices = $sub_service;
      $lead->leadcity = "Dhaka";
      $lead->orderdetails = $order_data;
      $lead->cname = $user_name;
      $lead->cphone = $user_phone;
      $lead->houseroad = $house_number . ", " . $street_address;
      $lead->source = "Android App";
      $lead->status = 1;
      $lead->leadarea = $area;

      //upload lead_img
      $lead_photo = $this->upload_img($request, "file", "lead_photo");

      if($lead_photo){
        //If user uploaded photo only then save it in db
        $lead->photos = $lead_photo;
      }


      $lead_saved = $lead->save();
      //Leads table==================================_end|

      if($lead_saved){
        //Data inserted into Leads table
        //Send sms verification code=========================Start
        if($is_client_exists === false){
          //only do the following if user is not authenticated/registered already
          $sms_result = $this->send_sms($user_phone, $client_pw, $verification_code);

        }
        //Send sms verification code=========================End

        //return $response->withStatus(200)->withHeader('Location', $sms_api_url);
        $create_new_lead_result['success'] = "Your order posted successfully";
        return $response->withStatus(200)->withJson($create_new_lead_result);


      }else{
        //For some reason data is not inserted in Leads table
        $create_new_lead_result['fail'] = "fail to post the order- please try again";
        return $response->withStatus(400)->withJson($create_new_lead_result);
      }



    }





  }
//create_new_lead ==============================End|

//verify_client ==============================Start|
  public function verify_client($request, $response){

    $data = $request->getParsedBody();


    $phone = $data['phone'];
    $verification_code = $data['verification_code'];



    if($request->getAttribute('has_errors')){
      //Validation failed==========================================
      $errors = $request->getAttribute('errors');
      return $response->withStatus(400)->withJson($errors);
    }else{
      //Validation Passed===========================================
      $verification_result = array();
      $verification_data = ['phone' => $phone, 'sms_code' => $verification_code];
      if (Client::where($verification_data)->exists()){
        //Client exists
        $verification_status = array();
        $verification_status = ['sms_verified' => 1];
        //update sms_verified col value in clients table
        Client::where($verification_data)->update($verification_status);
        /*TODO: Put a check here if update was successful then send the success responce*/
        $verification_result['success'] = "Client verification successfully done";
        return $response->withStatus(200)->withJson($verification_result);
      }else{
        //Client not found
        $verification_result['fail'] = "Client verification failed";
        return $response->withStatus(404)->withJson($verification_result);

      }


    }

  }
//verify_client ==============================End|

//user signin==============================Start|
public function user_signin($request, $response){

  $data = $request->getParsedBody();


  $phone = $data['phone'];
  $pass = md5($data['pass']);



  if($request->getAttribute('has_errors')){
    //Validation failed==========================================
    $errors = $request->getAttribute('errors');
    return $response->withStatus(400)->withJson($errors);
  }else{
    //Validation Passed===========================================
    $authentication_result = array();
    $authentication_data = ['phone' => $phone, 'password' => $pass];
    if (Client::where($authentication_data)->exists()){
      //Client exists

      //get client
      $current_user = Client::where($authentication_data)->first();
      //get client id
      $current_user_id = $current_user[id];
      $auth_token_arr = array();
      //make the token
      $token = bin2hex(openssl_random_pseudo_bytes(8));
      //token will expire after 1 week from now
      $token_expiration = date('Y-m-d H:i:s', strtotime('+1 week'));
      //generate $auth_token_arr for updating the currently loggedin client
      $auth_token_arr = ['token' => $token, 'token_expire' => $token_expiration];
      //update $auth_token_arr into authenticated client row
      Client::where($authentication_data)->update($auth_token_arr);
      $authentication_result['success'] = "Login success";
      $authentication_result['token'] = $token;
      $authentication_result['user_id'] = $current_user_id;
      return $response->withStatus(200)->withJson($authentication_result);
    }else{
      //Client not found
      $authentication_result['fail'] = "Login failed";
      return $response->withStatus(401)->withJson($authentication_result);

    }


  }

}
//user signin==============================End|

//get_myaccount==============================Start|
public function get_myaccount($request, $response){
  /* The request is authenticated through Authentication Middleware */

  $parsed_header = $this->parse_authorization_header($request);
  $user_id = $parsed_header['user_id'];

  $user = Client::where('id', '=', $user_id)->first();

  $user_phone = $request->getAttribute('phone');

  //Query for getting all finished booking by this client
  $finished_booking = Job::leftJoin('leads', 'jobs.orderid', '=', 'leads.orderid')
              ->where('jobs.status','13')->where('leads.cphone',$user_phone)->count();

  //Query for getting all unfinished booking by this client
  $unfinished_booking = Lead::leftJoin('inspections', 'leads.orderid', '=', 'inspections.oderid')
              ->leftJoin('jobs', 'leads.orderid', '=', 'jobs.orderid')
              ->where('leads.cphone',$user_phone)
              ->whereNotIn('leads.orderid', function($query)
                  {
                      $query->select('orderid')
                            ->from('jobs')
                            ->where('status', 13);
                  })
              ->where('leads.cphone',$user_phone)->count();






  $myaccount = array();

  //Generate the response
  $myaccount['cutomer_id'] = $user["id"];
  $myaccount['name'] = $user["firstname"] . " " . $user["lastname"];
  $myaccount['photo'] = $user["photo"];
  $myaccount['designation'] = $user["profession"];
  $myaccount['company'] = $user["company"];
  $myaccount['address'] = $user["street"] . ", " . $user["area"]. ", ". $user["city"];
  $myaccount['phone'] = $user["phone"];
  $myaccount['email'] = $user["email"];
  $myaccount['finished_booking'] = $finished_booking;
  $myaccount['unfinished_booking'] = $unfinished_booking;

  //Return response
  return $response->withStatus(200)->withJson($myaccount);
}
//get_myaccount==============================End|

//edit_myaccount==============================Start|
public function edit_myaccount($request, $response){
  /* The request is authenticated through Authentication Middleware */
  $data = $request->getParsedBody();

  $user_name = $data['user_name'];
  $user_email = $data['user_email'];
  $profession = $data['profession'];
  $street_address = $data['street_address'];
  $house_number = $data['house_number'];
  $concatenated_street = $house_number . ", " . $street_address;
  $area = $data['area'];
  $old_file = $data['old_img']; //users old image

  //$client_info will hold data that will be updated
  $client_info = array();
  $client_info = ['firstname' => $user_name, 'email'=> $user_email, 'profession'=> $profession, 'street'=> $concatenated_street, 'area'=> $area];

  //upload user img
  $user_photo_name = $this->upload_img($request, "file", "user_photo");

  if($user_photo_name){
    //If user uploaded photo only then save it in db
    $client_info['photo'] = $user_photo_name;
  }

  //Parse the Authorization header to get the user id that will be edited
  $parsed_header = $this->parse_authorization_header($request);
  //User id from parsed header
  $user_id = $parsed_header['user_id'];

  Client::where('id', $user_id)->update($client_info);

  //$edit_myaccount_result will hold success/failure response
  $edit_myaccount_result = array();
  $edit_myaccount_result['success'] = "Info updated successfully";

  //Delete old file

  if($user_photo_name && file_exists("user_photo/$old_file") ){
    //If new file uploaded and old file exist then only delete old file
    unlink("user_photo/$old_file");
  }



  //Return response
  return $response->withStatus(200)->withJson($edit_myaccount_result);

}
//edit_myaccount==============================End|

//get_finished_service==============================Start|
  public function get_finished_service($request, $response){
    /* The request is authenticated through Authentication Middleware */

    //Get the user phone from route param
    $user_phone = $request->getAttribute('phone');

    //Query for getting all finished booking by this client
    $finished_service = Job::leftJoin('leads', 'jobs.orderid', '=', 'leads.orderid')
                ->leftJoin('ratings', 'jobs.orderid', '=', 'ratings.orderid')
                ->leftJoin('taskers', 'jobs.assignedto', '=', 'taskers.tid')
                ->select('leads.services', 'ratings.clientrate', 'leads.created', 'leads.orderid', 'jobs.status', 'taskers.name')
                ->where('jobs.status','13')->where('leads.cphone',$user_phone)->get();

    //$finished_service_payload will hold each $finished_service data
    $finished_service_payload = array();

    //$i will hold array index value of $finished_service_payload['finished_services']
    $i = 0;
    foreach($finished_service as $row){

      $finished_service_payload['finished_services'][$i]["service"] = $row->services;
      $finished_service_payload['finished_services'][$i]["ratings"] = $row->clientrate;
      $finished_service_payload['finished_services'][$i]["time"] = $row->created;
      $finished_service_payload['finished_services'][$i]["orderid"] = $row->orderid;
      $finished_service_payload['finished_services'][$i]["ostatus"] = $this->order_status_number_to_text($row->status);
      $finished_service_payload['finished_services'][$i]["tasker"] = $row->name;

      $i++;
    }

    //Check for if there is no data found
    if(empty($finished_service_payload)){
      $finished_service_payload['finished_services'] = "No data found";
    }

    //Return response
    return $response->withStatus(200)->withJson($finished_service_payload);

  }
//get_finished_service==============================End|

//get_current_service==============================Start|
public function get_current_service($request, $response){
  /* The request is authenticated through Authentication Middleware */

  //Get the user phone from route param
  $user_phone = $request->getAttribute('phone');

  //Query for getting all unfinished booking by this client
  $current_service = Lead::leftJoin('inspections', 'leads.orderid', '=', 'inspections.oderid')
              ->leftJoin('jobs', 'leads.orderid', '=', 'jobs.orderid')
              ->leftJoin('taskers', 'jobs.assignedto', '=', 'taskers.tid')
              ->select('leads.services', 'leads.created as l_created', 'leads.orderid', 'leads.status as l_status', 'inspections.istatus as i_status', 'jobs.status as j_status', 'taskers.name')
              ->where('leads.cphone',$user_phone)
              ->whereNotIn('leads.orderid', function($query)
                  {
                      $query->select('orderid')
                            ->from('jobs')
                            ->where('status', 13);
                  })
              ->get();


              //$current_service_payload will hold each $current_service data
              $current_service_payload = array();

              //$i will hold array index value of $current_service_payload['current_services']
              $i = 0;
              foreach($current_service as $row){

                $current_service_payload['current_services'][$i]["service"] = $row->services;
                //$row->created was not working then i implemented this $row->l_created
                $current_service_payload['current_services'][$i]["time"] = $row->l_created;
                $current_service_payload['current_services'][$i]["orderid"] = $row->orderid;
                /*The max status value for a particular order between leads, inspections and jobs will be the current status of that order*/
                $current_service_payload['current_services'][$i]["ostatus"] = $this->order_status_number_to_text( max(array($row->l_status, $row->i_status, $row->j_status)) ) ;
                $current_service_payload['current_services'][$i]["tasker"] = $row->name;

                $i++;

              }

              //Check for if there is no data found
              if(empty($current_service_payload)){
                $current_service_payload['current_services'] = "No data found";
              }


              //Return response
              return $response->withStatus(200)->withJson($current_service_payload);
}
//get_current_service==============================End|

//save_tasker_rating_by_client==============================Start|
  public function save_tasker_rating_by_client($request, $response){
    /* The request is authenticated through Authentication Middleware */
    $data = $request->getParsedBody();


    $orderid = $data['orderid'];
    $clientrate = $data['clientrate'];
    $clientfeed = $data['clientfeed'];

    //Create the object for Rating Model
    $rating = new Rating();

    //Create php DateTime object
    $dt = new \DateTime;

    $rating->orderid = $orderid;
    $rating->clientrate = $clientrate;
    $rating->clientfeed = $clientfeed;
    $rating->crtime = $dt->format('g:i a | d/m/Y');

    //$rating_saved will hold boolean data if the model saved or not
    $rating_saved = $rating->save();

    //$save_rating_result will hold success/failure response
    $save_rating_result = array();
    if($rating_saved){
      //Data inserte into ratings table
      $save_rating_result["success"] = "Rated successfully";
      return $response->withStatus(200)->withJson($save_rating_result);
    }else{
      //For some unknown reason data is not inserted in ratings table
      $save_rating_result["fail"] = "Failure to save the ratting -please try again";
      return $response->withStatus(400)->withJson($save_rating_result);
    }

  }
//save_tasker_rating_by_client==============================End|

//get_tasker_profile==============================Start|
/*
  user will be able to view tasker's profile from finished_service and unfinished_service
*/
  public function get_tasker_profile($request, $response){
    /* The request is authenticated through Authentication Middleware */

    //Get the order_id from route param
    $order_id = $request->getAttribute('order_id');

    //$tasker_profile_payload will hold tasker_profile data
    $tasker_profile_payload = array();

    if(Job::where('orderid', $order_id)->whereIn('status', [11, 12, 13])->exists()){
      // Found
      //Query to get tasker profile data
      $tasker_data = Job::leftJoin('taskers', 'jobs.assignedto', '=', 'taskers.tid')
      ->select('taskers.tid', 'taskers.name', 'taskers.wcategory', 'taskers.experience', 'taskers.speciality', 'taskers.age', 'taskers.photo', 'taskers.bgcheck')
      ->where('jobs.orderid', $order_id)->first();

      //Query to get total_services count of a tasker
      $tasker_total_service_count = Job::where('assignedto', $tasker_data->tid)->count();

      //Generate $tasker_profile_payload
      $tasker_profile_payload['name'] = $tasker_data->name;
      $tasker_profile_payload['designation'] = $this->alphabetToServices($tasker_data->wcategory);
      $tasker_profile_payload['year_of_experience'] = $tasker_data->experience;
      $tasker_profile_payload['speciality'] = $tasker_data->speciality;
      $tasker_profile_payload['age'] = $tasker_data->age;
      $tasker_profile_payload['photo'] = $tasker_data->photo;
      $tasker_profile_payload['verification'] = $tasker_data->bgcheck;
      $tasker_profile_payload['total_services'] = $tasker_total_service_count;



    }else{
      //Not found
      $tasker_profile_payload['not_found'] = "Tasker is not assigned or order cancelled";

    }

    //Return response
    /*Tasker found or not found either one is a valid response so i am giving 200 for both of them-- Modify if needed*/
    return $response->withStatus(200)->withJson($tasker_profile_payload);

  }
//get_tasker_profile==============================End|

//reset_user_password==============================Start|
  public function reset_user_password($request, $response){

    $data = $request->getParsedBody();

    $user_phone = $data['phone'];


    //$password_reset_result will hold success/failure responce
    $password_reset_result = array();



    if (Client::where('phone', '=', $user_phone)->exists()){
      //The user exists so change the password now

      //Generate a random password for this client
      $client_pw = $this->random_password();
      $hashed_client_pw = md5($client_pw);

      $client_info = ['password' => $hashed_client_pw];

      Client::where('phone', $user_phone)->update($client_info);

      //send sms to user
      $sms_result = $this->send_sms($user_phone, $client_pw);

      $password_reset_result['success'] = "Password reset successfully";
      return $response->withStatus(200)->withJson($password_reset_result);

    }else{
      //No user found with this phone number
      $password_reset_result['fail'] = "Sorry, you need to register first";
      return $response->withStatus(403)->withJson($password_reset_result);

    }

  }
//reset_user_password==============================End|

//subscribe_user==============================Start|
public function subscribe_user($request, $response){

  $data = $request->getParsedBody();

  $user_name = $data['name'];
  $user_mobile = $data['mobile'];
  $user_email = $data['email'];

  //$subscribe_user_result will hold success/failure responce
  $subscribe_user_result = array();


  if (Subscribe::where('mobile', '=', $user_mobile)->exists()){
    $subscribe_user_result['fail'] = "You already subscribed";
    return $response->withStatus(409)->withJson($subscribe_user_result);//409 Conflict
  }else{

    $subscribe = new Subscribe();

    $subscribe->name = $user_name;
    $subscribe->mobile = $user_mobile;
    $subscribe->email = $user_email;
    $subscribe->offer = "ANDROIDSUB";

    //$subscribe_saved will hold boolean data if the model saved or not
    $subscribe_saved = $subscribe->save();

    if($subscribe_saved){
      $subscribe_user_result['success'] = "You subscribed successfully";
      return $response->withStatus(200)->withJson($subscribe_user_result);
    }else{
      //For some unknown reason data is not inserted in subscribe table
      $subscribe_user_result['fail'] = "Please try again";
      return $response->withStatus(500)->withJson($subscribe_user_result);
    }

  }

}
//subscribe_user==============================End|

/*=================Below are all generic function (e.g. following functions are not route handler)================*/
//alphabetToServices==============================Start|
  public function alphabetToServices($alpha) {
        switch ($alpha){
            case "A":
                return "Carpentry";
            case "B":
                return "Cleaning";
            case "C":
                return "Computer Servicing";
            case "D":
                return "Electrical";
            case "E":
                return "Home Appliances";
            case "F":
                return "Interior Design";
            case "G":
                return "Laundry Service";
            case "H":
                return "Pack & Shift";
            case "I":
                return "Painting";
            case "J":
                return "Pest Control";
            case "K":
                return "Plumbing";
            case "L":
                return "Property Management";

        }
    }
//alphabetToServices==============================End|

//order_status_number_to_text==============================Start|
/*
Purpose of method: table-tablerules contains different status of an order. each status is associated with an id.
this method will receive that id and will return text rulename.
$num_ostatus: numeric status of an order.
*/
public function order_status_number_to_text($num_ostatus) {
      $text_ostatus = Tablerule::select('rulename')->where('id', '=', $num_ostatus)->first();
      $text_ostatus = ucfirst($text_ostatus->rulename);
      return $text_ostatus;
  }
//order_status_number_to_text==============================End|

//upload_img==============================Start|
/*Purpose of method: This method will perform all image upload task in this projec
  $request: Request object
  $input_field_name: Form input field name that will contain the file
  $upload_destination: Where file will moved after upload

*/
  public function upload_img($request, $input_field_name, $upload_destination){
    $files = $request->getUploadedFiles();

    //If user not uploaded image then $files will be an empty array
    if(!$files){
      //If user not uploaded image then return false
      return false;
    }
    $newfile = $files[$input_field_name];
    $uploadFilename = $newfile->getClientFilename();


    $user_uploaded_file_name =substr($uploadFilename, 0, strpos($uploadFilename, '.'));
    $user_uploaded_file_ext =substr($uploadFilename, strpos($uploadFilename, '.'));

    $user_uploaded_file_name = round(microtime(true));
    $renamed_file = $user_uploaded_file_name.$user_uploaded_file_ext;

    $newfile->moveTo($upload_destination.'/'.$renamed_file);

    //=======Move to other directory====start
    //Implement code here when you will need to move photo to sytem directory-- from api directory
    //=======Move to other directory====End

    return $renamed_file;

  }
//upload_img==============================End|

//authenticate==============================Start|
  public function authenticate($token, $user_id){
    $authentication_data = ['id' => $user_id, 'token' => $token];
    if(Client::where($authentication_data)->where('token_expire', '>=', date('Y-m-d H:i:s'))->exists()){
      //**Client has authorized so update token expiration time**//
      //token will expire after 1 week from now
      $token_expiration = date('Y-m-d H:i:s', strtotime('+1 week'));//This date format should be similar to previous date format during token creation (signin)

      $token_expiration_data = ['token_expire' => $token_expiration];

      //update token_expire time
      Client::where($authentication_data)->update($token_expiration_data);
      return true;
    }

    return false;

  }
//authenticate==============================End|

//parse_authorization_header==============================Start|
/*Purpose of method: Every authenticated request will come with an Authorization header
  This method will parse that header. To follow the DRY principle this methode is defined.
  So any where in the project required to parese Authentication header they will use this method.
*/
public function parse_authorization_header($request){
  $auth = $request->getHeader('Authorization');
  $_auth = $auth[0];

  $parsed_header = array();

  $parsed_header['token'] = substr($_auth, strpos($_auth, ' '), strpos($_auth, '|'));
  $parsed_header['user_id'] = substr($_auth, strpos($_auth, '|')+1);


  return $parsed_header;
}

//parse_authorization_header==============================End|

//Generate password==============================Start|
  private function random_password() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890';
    $pass = array();
    $alphaLength = strlen($alphabet) - 1; //as array is 0 indexed
    for ($i = 0; $i < 5; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }
  //Generate password==============================End|

  //Generate Client verification code==============================Start|
    private function client_verification_code_generator() {
      $verification_code = mt_rand(10000, 99999);
      return $verification_code;
    }
    //Generate Client verification code==============================End|

    //send_sms==============================Start|
    public function send_sms($user_phone, $client_pw, $verification_code=false){
      $to =substr($user_phone, -11); //take the last 11 digit



      if($verification_code === false){
        /*This $custom_message is for user password reset*/
        $custom_message = "Use your phone number and password: '$client_pw' to login in your HandyMama account!";
      }else{
        /*This $custom_message is for user User registration during adding lead for the first time*/
        $custom_message="Your verification code is {$verification_code} next time you will use your phone number and password: '{$client_pw}' to login in your HandyMama account!";
      }

      $sms_api_url = "http://api.bulksms.top/2/?user=$this->sms_api_username&pass=$this->sms_api_pw&to=88$to&sender=YYYY&message=$custom_message";

      $url = str_replace(" ","%20",$sms_api_url);

      $ch = curl_init();
      $timeout = 5;
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
      $data = curl_exec($ch);
      curl_close($ch);

      return $data;
    }
    //send_sms==============================End|

}
