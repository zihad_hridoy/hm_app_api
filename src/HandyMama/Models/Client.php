<?php

namespace HandyMama\Models;

class Client extends \Illuminate\Database\Eloquent\Model
{

  
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $dateFormat = 'g:i a | d/m/Y';
    public function is_client_exists($user_phone){

      if (Client::where('phone', '=', $user_phone)->exists()) {
        // client found
        return true;
      }else{
        // Not found
        return false;
      }

  }

}
