<?php

namespace HandyMama\Models;

class Customer extends \Illuminate\Database\Eloquent\Model
{

  protected $primaryKey = 'cid';

  const CREATED_AT = 'created';
  const UPDATED_AT = 'updatetime';

  protected $dateFormat = 'g:i a | d/m/Y';


    /**
       * Check to see if customer already exists.
       *
       * @var string
       */
    public function is_customer_exists($user_phone){

      if (Customer::where('phone1', '=', $user_phone)->exists()) {
        // customer found
        return true;
      }else{
        // Not found
        return false;
      }

    }

    public function get_single_customer_by_column_name($col, $val){
      $customer = Customer::where($col, $val)->first();

      return $customer;

    }

}
