<?php

namespace HandyMama\Models;

class Rating extends \Illuminate\Database\Eloquent\Model
{
  public $timestamps = false;
}
