<?php

namespace HandyMama\Models;

class Subscribe extends \Illuminate\Database\Eloquent\Model{

  //As i am not following convention of making a table called "subscribes" for this model so i need to specify table name
  protected $table = 'subscribe';

  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';

  protected $dateFormat = 'g:i a | d/m/Y';
}
