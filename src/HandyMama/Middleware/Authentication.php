<?php
/*Add this Middleware for those rout that restricted */
namespace  HandyMama\Middleware;

use HandyMama\Controllers\Lead_controller;

class Authentication
{
  public function __invoke($request, $response, $next){


    $lead_controller = new Lead_controller();

    //Parse Authorization header
    $parsed_header = $lead_controller->parse_authorization_header($request);
    $token = $parsed_header['token'];
    $user_id = $parsed_header['user_id'];

    if($lead_controller->authenticate($token, $user_id)){
      //Request authenticated so let it access the protected resource
      $response = $next($request, $response);
      return $response;
    }else{
      //Failed to authenticate the request
      $authentication_result['failed'] = "You are not authorized to perform this action";
      return $response->withStatus(401)->withJson($authentication_result);


    }



  }
}
