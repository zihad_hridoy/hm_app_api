<?php

require 'vendor/autoload.php';
include 'bootstrap.php';

use Respect\Validation\Validator as v;
use HandyMama\Middleware\Authentication as hm_auth;



// Slim Settings
$config['displayErrorDetails'] = true;

$app = new \Slim\App(["settings" => $config]);

$container = $app->getContainer();

$container['Lead_controller'] = function(){
  return new \HandyMama\Controllers\Lead_controller;
};

$container['Test_controller'] = function(){
  return new \HandyMama\Controllers\Test_controller;
};

//================================================Rout:Testing Route
$routeParamValidator = v::stringType();
$validators = array(
  'name' => $routeParamValidator,
);


$app->get('/hello/{name}', 'Test_controller:testing_method')->add(new \DavidePastore\Slim\Validation\Validation($validators));
//================================================Rout:End

//================================================Rout:Add New Lead
//Create the validators================START|


$service_validator = v::alpha();
$sub_service_validator = v::alpha(',');
$order_data_validator = v::stringType();
$user_name_validator = v::alpha();
$user_phone_validator = v::phone();
$user_email_validator = v::email();
$street_address_validator = v::alnum(',-');
$house_number_validator = v::alnum(',-');
$area_validator = v::alnum(',-');

$validators = array(

  'service' => $service_validator,
  'sub_service' => $sub_service_validator,
  'order_data' => $order_data_validator,
  'user_name' => $user_name_validator,
  'user_phone' => $user_phone_validator,
  'user_email' => $user_email_validator,
  'street_address' => $street_address_validator,
  'house_number' => $house_number_validator,
  'area' => $area_validator

);
//Create the validators================END|

$app->post('/lead/new', 'Lead_controller:create_new_lead')->add(new \DavidePastore\Slim\Validation\Validation($validators));
//================================================Rout:End

//================================================Rout:[/lead/verify_client]
//Create the validators================START|

$phone_validator = v::phone();
$verification_code_validator = v::digit();


$validators = array(

  'phone' => $phone_validator,
  'verification_code' => $verification_code_validator

);
//Create the validators================END|

$app->post('/lead/verify_client', 'Lead_controller:verify_client')->add(new \DavidePastore\Slim\Validation\Validation($validators));
//================================================Rout:End

//================================================Rout:[/lead/user_signin]
//Create the validators================START|

$phone_validator = v::notEmpty();
$pass_validator = v::notEmpty();


$validators = array(

  'phone' => $phone_validator,
  'pass' => $pass_validator

);
//Create the validators================END|

$app->post('/lead/user_signin', 'Lead_controller:user_signin')->add(new \DavidePastore\Slim\Validation\Validation($validators));
//================================================Rout:End

//================================================Rout:[/myaccount/{phone}]
//Route for My account
// $routeParamValidator = v::stringType();
// $validators = array(
//   'id' => $routeParamValidator,
// );


$app->get('/myaccount/{phone}', 'Lead_controller:get_myaccount')->add(new hm_auth());
//================================================Rout:End

//================================================Rout:[/myaccount/edit]

$app->post('/myaccount/edit', 'Lead_controller:edit_myaccount')->add(new hm_auth());
//================================================Rout:End

//================================================Rout:[/user/current_service/{phone}]

$app->get('/user/current_service/{phone}', 'Lead_controller:get_current_service')->add(new hm_auth());
//================================================Rout:End

//================================================Rout:[/user/finished_service/{phone}]

$app->get('/user/finished_service/{phone}', 'Lead_controller:get_finished_service')->add(new hm_auth());
//================================================Rout:End

//================================================Rout:[/rate_tasker]

$app->post('/rate_tasker', 'Lead_controller:save_tasker_rating_by_client')->add(new hm_auth());
//================================================Rout:End

//================================================Rout:[/tasker_profile]

$app->get('/tasker_profile/{order_id}', 'Lead_controller:get_tasker_profile')->add(new hm_auth());
//================================================Rout:End

//================================================Rout:[/user/reset_password]

$app->post('/user/reset_password', 'Lead_controller:reset_user_password');
//================================================Rout:End

//================================================Rout:[/subscribe]

$app->post('/subscribe', 'Lead_controller:subscribe_user');
//================================================Rout:End

// Run app
$app->run();
