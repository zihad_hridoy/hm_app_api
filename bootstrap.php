<?php

include 'config/db_credentials.php';
include 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();
$capsule->addConnection([
  "driver" => "mysql",
  "host" => $db_host,
  "database" => $db_name,
  "username" => $db_user,
  "password" => $db_pass,
  "charset" => "utf8",
  "collation" => "utf8_general_ci",
  "prefix" => ""
]);

$capsule->bootEloquent();

//Set timezone --reference is the following url
//https://github.com/kladd/slim-eloquent/blob/master/app/config.php
date_default_timezone_set('Asia/Dhaka');
